<html>
    <head>
        <title>JCC Hari 1 Laravel</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="{{ route('welcome') }}" method="POST">
            @csrf
            <label>First name:</label>
            <br><br>
            <input type="text" name="namaDepan">
            <br><br>
            <label>Last name:</label>
            <br><br>
            <input type="text" name="namaBelakang">
            <br><br>

            <label>Gender:</label>
            <br><br>
            <input type="radio" name="gender" value="Male">
            <label>Male</label>
            <br>
            <input type="radio" name="gender" value="Feale">
            <label>Female</label>
            <br>
            <input type="radio" name="gender" value="Other">
            <label>Other</label>
            <br><br>
            
            <label>Language Spoken:</label>
            <br><br>
            <input type="checkbox" name="language" value="Bahasa Indonesia">
            <label>Bahasa Indonesia</label>
            <br>
            <input type="checkbox" name="language" value="English">
            <label>English</label>
            <br>
            <input type="checkbox" name="language" value="Other">
            <label>Other</label>
            <br><br>

            <label>Bio:</label>
            <br>
            <textarea rows="7"></textarea>
            <br><br>
            <button type="submit">Sign Up</button>
        </form>
        

    </body>
</html>
